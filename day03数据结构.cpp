#include<iostream>
#include<vector>
#include<string>
#include<assert.h>
using namespace std;
//删除链表中值为val 的结点： https://leetcode-cn.com/problems/shan-chu-lian-biao-de-jie-dian-lcof/submissions/
//class Solution {
//public:
//    ListNode* deleteNode(ListNode* head, int val) {
//        while (head) {
//            if (head->val != val) break;
//            head = head->next;
//        }
//
//        ListNode* prev = NULL;
//        ListNode* cur = head;
//        while (cur) {
//            if (cur->val == val) prev->next = cur->next;
//            else prev = cur;
//            cur = cur->next;
//        }
//
//        return head;
//    }
//};

//链表反转 ：https://leetcode-cn.com/problems/fan-zhuan-lian-biao-lcof/submissions/
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head) {
//        ListNode* cur = head;
//        ListNode* prev = NULL;
//        while (cur) {
//            ListNode* next = cur->next;
//            cur->next = prev;
//            prev = cur;
//            cur = next;
//        }
//
//        return prev;
//    }
//};

//数组模拟实现队列
template<class T>
class Mydueqe
{
public:
	Mydueqe(size_t limit = 5) 
		:_size(0)
		,_limit(limit)
		,_begin(0)
		, _end(0)

	{
		_arr = new T[limit];
	}
	void push(T val)  //队头进元素
	{
		if (_size == _limit) {
			assert(false);
		}		
		_size++;
		_arr[_end] = val;
		_end = nextindex(_end);
	}
	int pop()  //队尾出元素
	{
		if (_size == 0) {
			assert(false); 
		}
		_size--;
		int ret = _arr[_begin];
		_begin = nextindex(_begin);
		return ret;
	}
	int nextindex(size_t index) 
	{
		return index < _limit - 1 ? index + 1 : 0;
	}
	
private:
	T* _arr;  //队列空间
	size_t _size;  //个数
	size_t _limit; //限度
	size_t _begin; //起始
	size_t _end;   //结束
};
//双向链表实现队列和栈
struct Node
{
	Node(int val = 0) 
		:_prev(NULL)
		,_next(NULL)
		,_val(val)
	{	}
	Node* _prev;
	Node* _next;
	int _val;
};
class MyDdueqe 
{
public:
	MyDdueqe() 
		:_head(NULL)
		,_tail(NULL)
	{	}
	void push(int val) 
	{
		Node* newNode = new Node(val);
		if (_head == NULL) {
			_head = _tail = newNode;
			return;
		}
		newNode->_next = _head;
		_head->_prev = newNode;
		_head = newNode;
	}

	int pop() 
	{
		if (_head == _tail) return _tail->_val;
		Node* del = _tail;
		int ret = _tail->_val;
		_tail->_prev->_next = NULL;// 前一个结点的指针置空
		_tail = _tail->_prev;  //tail变成前一个结点
		
		delete del;  
		return ret; 
	}

	bool IsEmpty() 
	{
		return _head == _tail;
	}
private:
	Node* _head;
	Node* _tail;
};

class MyStack 
{
public:
	MyStack()
		:_head(nullptr)
	{ }
	void push(int val)
	{
		Node* newNode = new Node(val);
		if (_head == nullptr) {
			_head = newNode;
			_head->_next = NULL;
		}
		_head->_prev = newNode;
		newNode->_next = _head;
		_head = newNode;

	}
	void show() 
	{
		while (_head) {
			printf("%d ",  _head->_val);
			if (_head->_next == _head) {
				
			}
			_head = _head->_next;
		}
	}
	int pop() 
	{
		int ret = _head->_val;
		_head = _head->_next;
		return ret;
	}
private:
	
	Node* _head;
};

void Mydueqetest()
{
	MyDdueqe q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);
	q.push(6);
	int n = 6;
	while (n--) {
		printf("%d ",q.pop());
	}
}

void MyStacktest()
{
	MyStack st;
	st.push(1);
	st.push(2);
	st.push(3);
	st.push(4);
	st.push(5);
	st.push(6);
	st.show();
}
int main() 
{
	//Mydueqetest();
	MyStacktest();
	return 0;
}