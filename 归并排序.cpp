#include<iostream>
#include<vector>
#include<map>
using namespace std;

//求出【L ... R】区间的最大值
int dfs(int *a, int l, int r)
{
	if (l == r) return a[l];
	int mid = l + ((r - l) >> 1);
	int left = dfs(a, l, mid);
	int right = dfs(a, mid + 1, r);
	
	return max(left, right);
}

void merger(int l, int mid, int r,vector<int>& res) 
{
	int index = 0;
	int* tmp = new int[r - l + 1]; 
	int i = l;
	int j = mid + 1;
	while (i <= mid && j <= r)  tmp[index++] = res[i] <= res[j] ? res[i++] : res[j++];
	while (i <= mid) tmp[index++] = res[i++];
	while (j <= r) tmp[index++] = res[j++];


	for (int i = 0; i < r - l+ 1; i++) res[l + i] = tmp[i];
	
}

void process(int l, int r, vector<int>& res)
{
	if (l == r) return;
	int mid = l + ((r - l) >> 1);
	//分治递归
	process(l, mid, res);
	process(mid + 1, r, res);
	//归并
	merger(l, mid, r, res);
}

int main()	
{
	int a[] = { 5,1,32,5,2,6,59,66,11 };
	int n = sizeof(a) / sizeof(a[0]) ;
	vector<int> v(a, a + n);
	
	process(0, n - 1, v);
	for (auto& ref : v) cout << ref<<" ";
	return 0;
}