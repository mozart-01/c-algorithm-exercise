#include<iostream>
#include<vector>
#include<map>
using namespace std;


//归并排序
void merger(int l, int mid, int r,vector<int>& res) 
{
	int index = 0;
	int* tmp = new int[r - l + 1]; 
	int i = l;
	int j = mid + 1;
	while (i <= mid && j <= r)  tmp[index++] = res[i] <= res[j] ? res[i++] : res[j++];
	while (i <= mid) tmp[index++] = res[i++];
	while (j <= r) tmp[index++] = res[j++];	

	for (int i = 0; i < r - l+ 1; i++) res[l + i] = tmp[i];
	
}

void mergerNoR(int l, int r, vector<int>& v)
{
	if (v.empty() || v.size() < 2) return;
	int N = v.size();
	int gap = 1;
	while (gap < N)
	{
		int L = 0;
		while (L < N)   
		{
			int M = L + gap - 1;  //确定右组
			if (M >= N) break;  //如果右组超过了N那么就跳过
			int R = min(M + gap, N - 1);
			merger(L, M, R, v);
			L = R + 1;
		}
		gap <<= 1;
	}
}


void test() 
{
	int a[] = { 5,1,32,5,2,6,59,66,11 };
	int n = sizeof(a) / sizeof(a[0]);
	vector<int> v(a, a + n);

	mergerNoR(0, n, v);
	for (auto& ref : v) cout << ref << " ";
}
int main()	
{
	test();
	
	return 0;
}