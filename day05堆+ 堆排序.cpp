#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<vector>
#include<assert.h>
#include<algorithm>
using namespace std;

class student 
{
	
public:
	struct Comper
	{
		bool operator()(student& stu1, student& stu2)
		{
			return stu1._id < stu2._id;
		}
	};

	student(const char *name, int id,int age)
		: _id(id)
		,_age(age)
	{
		strcpy(_name, name);
	}
	friend ostream& operator<<(ostream& out, student& stu)
	{
		out << stu._name << "," << stu._id << "," << stu._age << endl;
		return out;
	}
private:
	char _name[20];
	int _id;
	int _age;
};

static void func() 
{
	vector<student> v;
	v.push_back(student("中山", 11, 20));
	v.push_back(student("中山", 120, 18));
	v.push_back(student("中山", 9, 16));
	sort(v.begin(), v.end(), student::Comper());

	for (auto &ref : v) {
		cout << ref ;
	}	
}

namespace mzt 
{
	//大根堆
	template<class T>
	class greater   
	{
	public:
		bool operator()(T &val1, T& val2)
		{
			return val1 > val2; 
		}
	};

	//小根堆
	template<class T>
	class less
	{
	public:
		bool operator()(T& val1, T& val2)
		{
			return val1 < val2; 
		}
	};

	template<class T, class COMP>
	class help
	{
	public:
		typedef T* Iterator;
		Iterator begin() { return _arr; }
		Iterator end() { return _arr + _size; }

		help()
			: _size(0)
			, _capacity(4)
		{
			_arr = new T[4];
			memset(_arr, 0, sizeof(T) * 4);
		}
		void reserve(size_t n)
		{
			if (n > _capacity) {
				T* tmp = new T[n];
				memcpy(tmp, _arr, sizeof(T) * n);
				delete[]_arr;
				_arr = tmp;
				_capacity = n;
			}
		}

		//向下调整算法
		void helpify(int arr[], int parent, size_t len)
		{
			COMP cmp;
			//计算左孩子
			size_t child = parent * 2 + 1;
			while (child < len) {  //左孩子越界右孩子就必越界了。
				//选出左右孩子最大的那一个
				int largest = (child + 1 < _size && cmp(_arr[child + 1] , _arr[child]))
					          ? child + 1 : child;
				
				largest = cmp(arr[largest] , _arr[parent]) ? largest: parent;
				if (largest == parent) break;  
				swap(_arr[largest], _arr[parent]);
				
				//调整父亲和孩子的位置
				parent = largest;
				child = parent * 2 + 1;
			}
		}

		void helpsort() 
		{
			//建堆
			for (int i = (size() - 1 - 1) / 2; i >= 0; i--) {
				helpify(_arr, i, _size);
			}
			//堆排序
			int end = size() - 1;
			while(end > 0)
			{
				swap(_arr[0], _arr[end]);
				end--;
				helpify(_arr, 0, end);
			}
			
			
		}
		//插入值并向上调整
		void healpInsert(const T& val, size_t index)
		{
			COMP cmp;
			//扩容
			if (_size == _capacity) {
				reserve(_capacity == 0 ? 4 : _capacity * 2);
			}
			//向上调整
			_arr[_size] = val;
			while (index != 0 && cmp(_arr[index] , _arr[(index - 1) / 2])) {  //向上调整算法
				swap(_arr[index], _arr[(index - 1) / 2]);
				index = (index - 1) / 2;  //更新index的位置，比较当前值与上一个父亲
			}
			_size++;
		}
		bool empty() { return _size == 0; }
		int top() //获取堆顶的值
		{
			assert(!empty());
			return _arr[0];
		}
		//弹出堆顶的值
		void pop() 
		{
			assert(!empty());
			swap(_arr[0], _arr[_size - 1]); //头尾交换
			_size--;
			//从0位置向下调整
			helpify(_arr, 0);
		}
		size_t size() {
			return _size;
		}
	private:
		T* _arr;
		size_t _size;
		size_t _capacity;
	};

	void test() 
	{
		help<int, mzt::greater<int>> h;
		h.healpInsert(1, h.size());
		h.healpInsert(2, h.size());
		h.healpInsert(3, h.size());
		h.healpInsert(4, h.size());
		h.healpInsert(5, h.size());
		h.healpInsert(6, h.size());
		h.helpsort();
		for (auto &ref: h) {
			cout << ref << " ";
		}
		cout << endl;

		/*h.top();
		h.pop();
		for (auto& ref : h) {
			cout << ref << " ";
		}
		cout << endl;
		h.top();
		h.pop();
		for (auto& ref : h) {
			cout << ref << " ";
		}
		cout << endl;*/
		
	}
}

//int main() 
//{
//	mzt::test();
//	//func();
//	return 0;
//}


void dfs(int arr[], int len)
{
	
}