#include<iostream>
#include<vector>
using namespace std;

//前缀树：https://leetcode-cn.com/problems/implement-trie-prefix-tree/
struct Node {
	size_t _pass;
	size_t _end;
	vector<Node*> _v;
	Node() :_pass(0), _end(0) { _v.resize(26, nullptr); }
};
class Trie {
public:
	Node* _root;
	Trie() {
		_root = new Node();
	}

	void insert(string word) {
		if (word.empty() || word == "") return;
		int path = 0;
		Node* node = _root;
		for (size_t i = 0; i < word.size(); i++) {
			path = word[i] - 'a';
			if (node->_v[path] == nullptr) {
				node->_v[path] = new Node();
				node->_v[path]->_pass++;
			}
			node = node->_v[path];
		}
		node->_end++;
	}

	bool search(string word) {
		if (word.empty() || word == "") return false;
		int path = 0;
		Node* node = _root;
		for (size_t i = 0; i < word.size(); i++) {
			path = word[i] - 'a';
			if (node->_v[path] == nullptr) {
				return false;
			}
			node = node->_v[path];
		}
		return node->_end >= 1 ? true : false;
	}

	bool startsWith(string prefix) {
		if (prefix.empty() || prefix == "") return false;
		int path = 0;
		Node* node = _root;
		for (size_t i = 0; i < prefix.size(); i++) {
			path = prefix[i] - 'a';
			if (node->_v[path] == nullptr) {
				return false;
			}
			node = node->_v[path];
		}
		return true;
	}
};

/**
 * Your Trie object will be instantiated and called as such:
 * Trie* obj = new Trie();
 * obj->insert(word);
 * bool param_2 = obj->search(word);
 * bool param_3 = obj->startsWith(prefix);
 */
namespace mzt 
{
	
	struct Node 
	{
		size_t _pass;
		size_t _end;
		Node** _next;
		Node()
			:_pass(0)
			,_end(0)
			,_next(new Node*[26]) //建立存放26个指针的数组
		{
			for (size_t i = 0; i < 26; i++) {
				_next[i] = nullptr;
			}
		}
	};

	class prefixtree 
	{
		typedef struct Node Node;
	public:
		prefixtree() :_root(new Node()){}

		//删除前缀字符串， 1、pass为0的时候直接将该结点释放 2、还剩余结点的话只需要计数--
		void delNode(const string &prev) 
		{
			if (search(prev) == 0) return;  //没有出现过
			Node* node = _root;
			node->_pass--;  //遍历根节点的时候需要--
			int path = 0;
			for (size_t i = 0; i < prev.size(); i++) {
				path = prev[i] - 'a';
				if (--(node->_next[path]->_pass) == 0) {
					Node* null = node->_next[path];
					size_t j = i;
					while (j < prev.size() && node->_next[path] != nullptr) {
						Node* del = node->_next[path];
						path = prev[j++];
						node = node->_next[path];
						delete del;
					}
					null = nullptr;
					return;
				}
			}
			node->_next[path]->_end--;
		}

		void insert(const string& str) 
		{
			if (str.empty() || str == "") return ;
			Node* node = _root;
			for (size_t i = 0; i < str.size(); i++) {
				int path = str[i] - 'a';
				if (node->_next[path] == nullptr) {
					node->_next[path] = new Node();
				}
				node = node->_next[path];  //更新node的位置
				node->_pass++;//新建结点pass++
			}
			node->_end++;  //记录字符串的结束位置
		}

		//所加入的字符串中有几个是以prev作为前缀出现在树中的
		size_t search(const string& prev) 
		{
			if (prev.empty() || prev == "") return 0; 
			Node* node = _root;
			
			for (size_t i = 0; i < prev.size(); i++) {
				int path = prev[i] - 'a';
				if (node->_next[path] == nullptr) {
					return 0;  //表示没有出现过
				}
				node = node->_next[path];  //出现一次匹配继续往下查找
			}
			return node->_pass; //返回以prev作为前缀出现的次数
		}

	private:
		Node *_root;
	};
	static void test()
	{
		prefixtree tree;

	}

	//计数排序
	int getmaxdig(vector<int>& v) 
	{
		int max_val = 0;
		for (auto &ref : v) {
			max_val = max(max_val, ref);
		}
		int res = 0;
		while (max_val) {
			res++;
			max_val /= 10;
		}
		return res;
	}
	int getdigit(int& val, int d) { return val / ((int)pow(10, d - 1)) % 10; }
	void bucketsort(vector<int>&v, int L, int R, int digit) 
	{
		vector<int> help;
		help.resize(R - L + 1);
		const int radix = 10;
		
		for (int d = 1; d <= digit; d++) {
			vector<int> count;
			count.resize(R - L + 1);
			for (size_t i = 0; i < v.size(); i++) {
				int j = getdigit(v[i], d);
				count[j]++;  
			}
			for (int i = 1; i < radix; i++) {
				count[i] = count[i - 1] + count[i];
			}
			for (int i = R; i >= L; i--) {
				int j = getdigit(v[i], d);
				help[count[j] - 1] = v[i];
				count[j]--;
			}
			int index = 0;
			for (auto &ref : help) {
				v[index++] = ref;
			}
		}

	}
	void countsort(vector<int> &v) 
	{
		if (v.empty()) return;
		bucketsort(v, 0, v.size() - 1, getmaxdig(v));
		for (auto &ref : v) {
			cout << ref << " ";
		}
	}

}

void testcount() 
{
	int arr[] = {11,2,5,6,9,20,3,1,47,58};
	vector<int> v(arr, arr + sizeof(arr) / sizeof(arr[0]));
	mzt::countsort(v);
	
}
int main() 
{
	testcount();
	//mzt::test();
	return 0;
}