#pragma once
#include<iostream>
#include<vector>
#include<map>
#include<unordered_set>
#include<stdio.h>
using namespace std;
static void swap(int arr[], int i, int j) //注意： 异或的话不能和同一块地址的值异或，
												//不然就会被刷成0
{
	arr[i] = arr[i] ^ arr[j];
	arr[j] = arr[i] ^ arr[j];
	arr[i] = arr[i] ^ arr[j];
}

static void insertsort(int arr[], int len) 
{
	int l = 0;
	int r = len - 1;
	for (int i = 1; i < len; i++) {
		for (int j = i - 1; j >= 0 && arr[j] >= arr[j + 1]; j--) {
			swap(arr, j, j + 1);
		}
	}
}
static void  insertsorttest() 
{
	int arr[] = {5,6,21,4,3,2,1,};
	insertsort(arr, sizeof(arr) / sizeof(arr[0]));
	for (auto& ref : arr) printf("%d ", ref);
}
//不创建临时变量交换两个数
static void swap(int &x, int &y)
{
	if (&x == &y) return ;
	x = x ^ y;
	y = x ^ y;
	x = x ^ y;
}
static void test()
{
	int a = 7;
	int b = 13;
	swap(a, b);
	printf("a: %d b: %d", a, b);
}

//找出数组中的奇数数字
static void showodd(int arr[], int len) 
{
	int ans = 0;
	for (int i = 0; i < len; i++) {
		ans ^= arr[i];
	}
}
//  a    b    c
//数组中两个数出现了奇数次，其他数出现了偶数次，请你找到并打印这两个数
static void findsingle(int arr[], int len) 
{
	int x = 0;
	int pos = -1;
	for (int i = 0; i < len; i++)  x ^= arr[i];   //x去异或完数组后，x = a ^ b; 
	for (int i = 0; i <= 31; i++) {
		if ((x << i) & 1) { pos = i;  break; }
	}
	int ans1 = 0, ans2 = 0;
	for (int i = 0; i < len; i++) {
		if ((arr[i] << pos) & 1) ans1^= arr[i];
		else ans2 ^= arr[i];
	}
	printf("%d %d", ans1, ans2);
}
void findsingletest1()
{
	int arr[] = {5,2,6,2,4,4,5,7};
	findsingle(arr, sizeof(arr) / sizeof(arr[0]));
}
//左神版本实现单身狗
void findsingle1_zs(int arr[], int len)
{
	int eor = 0;
	for (int i = 0; i < len; i++) eor ^= arr[i];  //找到a ^ b的结果
	int pos = eor ^ (-eor); //寻找eor最右侧为1的数字
	int ans = 0;
	for (int i = 0; i < len; i++) {
		if ((arr[i] ^ pos) & 1) ans ^= arr[i]; //将pos位置值不等的异或到一块, 找到b
	}
	printf("%d %d",ans, ans ^ eor); //打印b 和 a ,  ans ^ eor == (a^b)^b == a
}
void findsingletest2() 
{
	int arr[] = { 5,2,8,2,4,4,5,7 };
	findsingle1_zs(arr, sizeof(arr) / sizeof(arr[0]));
}

//寻找数组中出现k次的数组，注意数组中除了一个数字出现了k次，其他数都出现了m次，
//并且k < m && m > 1
int oncek(int arr[], int len ,int k ,int m) 
{
	int t[32] = { 0 };  //初始化数组
	int index = 0;
	for (int i = 0; i < len; i++) {
		for (int j = 0; j <= 31; j++) {
			t[j] += ((arr[i] >> j) & 1);  //判断数组中的数是否会在此出现过
		}
	}
	int ans = 0;
	for (int i = 0; i <= 31; i++) {
		if ((t[i] % m) != 0) { //如果数组中存储的位置值并不是m的整数倍，那
							   //么必然会出现一次出现k数次的1
			ans |= (1 << i);
		}
	}
	return ans;
}
//相对映射
static int relative(int arr[], int len, int k, int m)
{
	
	int max = -1;
	int min = 10000;
	for (int i = 0; i < len; i++) {
		if (max < arr[i])  max = arr[i];
		if (min > arr[i])  min = arr[i];
	}
	
	int count[1000] = { 0 };
	for (int i = 0; i < len; i++) {
		count[arr[i] - min]++;
	}
	for (int i = 0; i < len; i++) {
		if (count[arr[i] - min] == k) return arr[i];
	}
	return -1;
}

static void oncektest() 
{
	int arr[] = { -1,3,1,3,3,1,1,-1 };
	int len = sizeof(arr) / sizeof(arr[0]);
	int k = 2;
	int m = 3;
	//int ret = oncek(arr, sizeof(arr) / sizeof(arr[0]), k, m);
	int ret = relative(arr, len, k, m);
	printf("%d ",ret);
}
int randomArray(int range)
{
	return ((int)(rand() % range) + 1) - ((int)(rand() % range) + 1);
}
//初始化数组
int* randArray(int max, int range, int k, int m, int* len)
{
	//获取随机数
	int ktimeNum = randomArray(range);
	int numsKinds = (int)(rand() % ktimeNum) + 2; //数字种类
	//全部的数等于k * 1 + (numskinds - 1) * m
	int* tmp = new int[k + (numsKinds - 1) * m];//创建数组长度
	int index = 0;
	unordered_set<int> set;
	set.insert(ktimeNum);
	for (; index < k;) {
		tmp[index++] = ktimeNum;  //填充k个为ktimeNum的数字
	}
	numsKinds--;

	while (numsKinds--)
	{
		int record = 0;
		do
		{
			record = randomArray(range);
		} while (set.insert(record).second == false);

		for (int i = 0; i < m; i++) {
			tmp[index++] = record;
		}
	}
	*len = index;
	return tmp;
}
//对数器
void logarithm()
{
	int kinds = 10; 
	int range = 200;
	int testtime = 200;  //运行时间
	int max = 9;  //最大数

	int* test1 = NULL;
	printf("测试开始\n");
	for (int i = 0; i < testtime; i++) {
		int a = (int)(rand() % max) + 1; //获取随机数
		int b = (int)(rand() % max) + 1;
		// 因为k < m
		int k = std::min(a, b);
		int m = std::max(a, b);
		if (k == m) m++;

		int len = 0;
		test1 = randArray(kinds, range, k, m, &len);
		int ans1 = oncek(test1, len, k, m);
		int ans2 = relative(test1, len, k, m);
		if (ans1 != ans2) printf("错误\n");
	}

	delete test1;
	printf("测试结束\n");
}

int main()
{
	std::srand((unsigned int)time(NULL));  //定义时间种子
	//findsingletest();
	//findsingletest2();
	logarithm();
	
	return 0;
}